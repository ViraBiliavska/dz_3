//
//  ViewController.swift
//  ДЗ_3
//
//  Created by Mac on 16.07.2018.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

    print("БЛОК 1 ЗАДАЧА 0\n")
    
    searchLargestNumber(numberTwo: Int(arc4random()%1000))
        
        print("________________________\n")
        print("БЛОК 1 ЗАДАЧА 1\n")
        
        squareOfNumber(number: 7)
        cubeOfNumber(number: 80)
        
        let a = 5.0
        let b = 3.0
        squareOfNumber(number: a)
        cubeOfNumber(number: b)
        
        print("________________________\n")
        print("БЛОК 1 ЗАДАЧА 2\n")
        
        range(number: Int(arc4random()%15))
        
        print("________________________\n")
        print("БЛОК 1 ЗАДАЧА 3\n")
        
        searchForNumberDivisors(delimoe: Int(arc4random()%120))

        print("________________________\n")
        print("БЛОК 1 ЗАДАЧА 4\n")
        
        checkIdealNumber(delimoe: Int(arc4random()%120))
        
        print("________________________\n")
        print("БЛОК 2 ЗАДАЧА 1\n")
        
        interestCapitalization(startingPrice: 24.0, year: 1826, yearNow: 2018, percent: 6.0)
        
        print("________________________\n")
        print("БЛОК 2 ЗАДАЧА 2\n")
        
        requiredAmount()
//        или вывести сюда все, что находится в requiredAmount()
        
        print("________________________\n")
        print("БЛОК 2 ЗАДАЧА 3\n")
        
        searchOfPeriod(saving: 2400.0, expensesMonthly: 1000.00, inflationRate: 3.0, scholarship: 700.0)
        
        print("________________________\n")
        print("БЛОК 2 ЗАДАЧА 4\n")
        
        inversion(number: 123456789)
    }
    
    func searchLargestNumber (numberOne: Int = 500, numberTwo: Int) -> Int {
        print("Первое число равно \(numberOne)")
        print("Второе число равно \(numberTwo)")
        if numberOne > numberTwo {
            print("Число \(numberOne) больше чем \(numberTwo)")
        } else if numberOne < numberTwo {
            print("Число \(numberTwo) больше чем \(numberOne)")
        } else if numberOne == numberTwo {
            print("Числа нельзя ставнить, они равны")
        }
        return numberTwo
    }
    
    func squareOfNumber(number: Double) -> Double {
        let doubleNumber = pow(number, 2)
        print("Квадрат \(number) равен \(doubleNumber)")
        return number
    }
    
    func cubeOfNumber(number: Double) -> Double {
        let cubeNumber = pow(number, 3)
        print("Куб \(number) равен \(cubeNumber)")
        return number
    }
    
    func range(number: Int) -> Int {
        print("Число =", number)
        for i in 0...number {
            print(i)
        }
        print("####")
        for a in 0...number {
            print(number - a)
        }
        return number
    }
    
    func searchForNumberDivisors(delimoe: Int) -> Int {
        var kolichestvo = 0
        var delitel: Int
        print("Число \(delimoe) имеет такие делители:")
        for delitel in 2...delimoe / 2 {
            if (delimoe % delitel == 0) {
                print (delitel)
                kolichestvo += 1
            }
        }
        print ("Количество делителий числа \(delimoe) равно \(kolichestvo)")
        return delimoe
    }
    
    func checkIdealNumber (delimoe: Int) {
        var delitelNew: Int
        var summa: Int = 0
        
        print("Делители числа \(delimoe):")
        
        for delitelNew in 2...delimoe / 2 {
            if (delimoe % delitelNew == 0) {
                print (delitelNew)
                summa = summa + delitelNew
            }
        }
        print ("Сумма делителий числа \(delimoe) равна \(summa)")
        if (summa == delimoe) {
            print("Число \(delimoe) является совершенным")
        } else {
            print("Число \(delimoe) не является совершенным")
        }
    }
    
    func interestCapitalization (startingPrice: Double,
                                 year: Int,
                                 yearNow: Int,
                                 percent: Double) {
        var summaNow = 0.00
        var summa = 0.00
        summa = startingPrice
        for period in year..<yearNow {
            summaNow = summa + (summa * percent / 100)
            summa = summaNow
        }
        print("Ответ: $", summa)
    }
    
    func incomes (scholarship: Double, numberOfMounths: Int) -> Double {
        var incomeMonthly: Double = 0.0
        var totalIncome: Double = 0.0
        for period in 0..<numberOfMounths {
            incomeMonthly = totalIncome + scholarship
            totalIncome = incomeMonthly
        }
        print("За \(numberOfMounths) месяцев доходы студента составят \(totalIncome)")
        return totalIncome
    }
    
    func expenses (expensesMonthly: Double, inflationRate: Double, numberOfMounths: Int) -> Double {
        var totalExpenses: Double = 0.0
        var accuredExpenses: Double = expensesMonthly
        for period in 0..<numberOfMounths - 1 {
            accuredExpenses = accuredExpenses + accuredExpenses * inflationRate / 100
            totalExpenses = totalExpenses + accuredExpenses
        }
        totalExpenses = totalExpenses + expensesMonthly
        print("За \(numberOfMounths) месяцев расходы студента составят \(totalExpenses)")
        return totalExpenses
    }
    
    func requiredAmount() {
        var requiredAmount = incomes(scholarship: 700, numberOfMounths: 10) - expenses(expensesMonthly: 1000, inflationRate: 3.0, numberOfMounths: 10)
        if requiredAmount > 0 {
            print("У студента останется \(requiredAmount)")
        } else {
            print("Студенту нужно еще \(-requiredAmount)")
        }
    }
    
    func searchOfPeriod(saving: Double, expensesMonthly: Double, inflationRate: Double, scholarship: Double) {
        var costs: Double = expensesMonthly
        var incomes: Double = 0.0
        var sum: Double = 0.0
        var difference: Double = 0.0
        for monthNumber in 1..<10 {
            costs = costs + costs * inflationRate / 100
            sum = sum + costs
            incomes = incomes + scholarship
            difference = sum - incomes
            //            print(incomes, sum, difference, monthNumber)
            if difference > saving {
//                print("В \(monthNumber) месяце расходы студента \(difference) превысят его сбережения")
                print("Денег хватит на \(monthNumber - 1) месяцев")
                break
            }
        }
    }
    
    func inversion(number: Int) {
        print(number)
        var chastnoe: Int = number
        var ostatok: Int = 1
        var new: Int = 0
        new = number % 10
        for _ in 0...number {
            while chastnoe > 10 {
                chastnoe = chastnoe / 10
                new = new * 10
                ostatok = chastnoe % 10
                new = new + ostatok
//                print(new)
            }
        }
        print(new)
    }
}
        
        

    




